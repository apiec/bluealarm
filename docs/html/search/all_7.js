var searchData=
[
  ['infantry_32',['Infantry',['../class_infantry.html',1,'']]],
  ['inputmanager_33',['InputManager',['../class_input_manager.html',1,'']]],
  ['isdead_34',['isDead',['../class_game_entity.html#adaea47e3b3d0503eef46cc4f54b1c49e',1,'GameEntity']]],
  ['ispassable_35',['isPassable',['../class_terrain_tile.html#a6021c59fb00cdd70b5e2ca1f7054720b',1,'TerrainTile']]],
  ['ispassableat_36',['isPassableAt',['../class_game_map.html#a2d9f691de1731d6b6ca4163342e7c5a5',1,'GameMap::isPassableAt(uint64_t pos, uint64_t width=0) const'],['../class_game_map.html#ac5d2308dd6052f27cec0beb8f26b3f0c',1,'GameMap::isPassableAt(uint64_t x, uint64_t y, uint64_t width) const']]],
  ['isproducing_37',['isProducing',['../class_production_queue.html#ab2d1595716e12a79522cde821484d0cf',1,'ProductionQueue']]]
];
