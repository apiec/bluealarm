#include "EventDispatcher.h"

EventDispatcher::EventDispatcher(sf::RenderWindow* window) :
	window_(window)
{
	callbackVectors_.reserve(TEventType::Count);
	for (int i = 0; i < TEventType::Count; ++i)
	{
		callbackVectors_.emplace_back(TCallbackVector());
	}
}

EventDispatcher::~EventDispatcher()
{
}

#pragma warning( push )
#pragma warning( disable : 26812 )
//disabling Warning C26812: Prefer 'enum class' over 'enum' (Enum.3)
//we're actually using the fact that an enum converts to an int easily
//using Visual Studio preprocessor commands
void EventDispatcher::connect(TEventType eventType, TCallbackFn& callback)
{
	callbackVectors_[eventType].push_back(callback);
}
void EventDispatcher::connect(TEventType eventType, TCallbackFn&& callback)
{
	callbackVectors_[eventType].push_back(callback);
}
#pragma warning( pop )

void EventDispatcher::loop()
{
	sf::Event event;
	while (window_->pollEvent(event))
	{
		dispatchEvent(event);
	}
}

void EventDispatcher::dispatchEvent(const sf::Event& event) const
{
	for (auto callback : callbackVectors_[event.type])
	{
		callback(event);
	}
}