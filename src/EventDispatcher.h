#pragma once

#include <vector>
#include <functional>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "Loopable.h"

/// @brief Used to poll for SFML events and notify other objects of them.
class EventDispatcher : public Loopable
{
public:
	explicit EventDispatcher(sf::RenderWindow* window);
	~EventDispatcher();

	using TEventType = sf::Event::EventType;
	using TCallbackFn = std::function<void(const sf::Event&)>;
	void connect(TEventType eventType, TCallbackFn& callback);
	void connect(TEventType eventType, TCallbackFn&& callback);

	void loop() override;

private:

	void dispatchEvent(const sf::Event& event) const;

	sf::RenderWindow* window_;

	using TCallbackVector = std::vector<TCallbackFn>;
	std::vector<TCallbackVector> callbackVectors_;
};

