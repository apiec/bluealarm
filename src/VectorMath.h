#pragma once

#include <SFML/System/Vector2.hpp>
#include <cmath>

/// Provides some basic vector math to be used on sf::Vector2f objects
namespace VectorMath
{
	inline float constexpr PI = 3.14159265f;

	inline float lengthSquared(const sf::Vector2f vec)
	{
		return vec.x * vec.x + vec.y * vec.y;
	}

	inline float length(const sf::Vector2f vec)
	{
		return std::sqrtf(lengthSquared(vec));
	}

	inline sf::Vector2f normalize(const sf::Vector2f vec)
	{
		return vec / length(vec);
	}

	inline sf::Vector2f rotate(const sf::Vector2f vec, float angle, bool rad = false)
	{
		if (!rad)
		{
			angle *= PI / 180.f;
		}
		auto cs = std::cosf(angle);
		auto sn = std::sinf(angle);
		return sf::Vector2f{cs * vec.x - sn * vec.y, sn * vec.x - cs * vec.y };
	}

	inline float angle(const sf::Vector2f vec)
	{
		if (vec.x < 0)
			return 180.f + std::atanf(vec.y / vec.x) * 180.f / PI;
		else
			return std::atanf(vec.y / vec.x) * 180.f / PI;
	}

	inline bool areCollinear(const sf::Vector2f vec1, const sf::Vector2f vec2)
	{
		return angle(vec1) - angle(vec2) < std::numeric_limits<float>::epsilon();
	}

	template <typename T>
	inline int sgn(T val) {
		return (T(0) < val) - (val < T(0));
	}
}

