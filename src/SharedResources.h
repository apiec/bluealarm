#pragma once

#include <vector>
#include <unordered_set>


#include "Managers/AssetManager.h"
#include "Managers/InputManager.h"
#include "GameObjects/GameMap.h"
#include "Managers/EntityManager.h"


/// @brief This class holds resources and functionalities commonly used by Entities throught the project.
struct SharedResources
{
	InputManager inputManager;
	AssetManager assets;
	GameMap gameMap;
	EntityManager entityManager;
};

