#pragma once


/// @brief An interface for loopable managers.
class Loopable
{
public:
	virtual ~Loopable() {}
	/// Called every game loop.
	virtual void loop() = 0;
};