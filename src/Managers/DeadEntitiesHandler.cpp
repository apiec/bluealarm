#include "DeadEntitiesHandler.h"

DeadEntitiesHandler::DeadEntitiesHandler(SharedResources& shr) :
	shr_(shr)
{
}

DeadEntitiesHandler::~DeadEntitiesHandler()
{
}

void DeadEntitiesHandler::addDeadEntity(GameEntity* deadEntity)
{
	deadEntities_.emplace(deadEntity, sf::Clock());
}

void DeadEntitiesHandler::update()
{
	checkForEntityExpiration();
	removeExpiredEntitiesFromGame();
}

void DeadEntitiesHandler::checkForEntityExpiration()
{
	for (auto entityClockPair : deadEntities_)
		if (entityClockPair.second.getElapsedTime().asSeconds() > EXPIRATION_TIME_IN_SECONDS)
			expireEntity(entityClockPair.first);
}

void DeadEntitiesHandler::expireEntity(GameEntity* entity)
{
	expiredEntities_.emplace_back(entity);
}

void DeadEntitiesHandler::removeExpiredEntitiesFromGame()
{
	for (auto entity : expiredEntities_)
	{
		deadEntities_.erase(entity);
		shr_.entityManager.removeAndDeleteEntity(entity);
	}
	expiredEntities_.clear();
}