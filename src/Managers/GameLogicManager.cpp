#include "GameLogicManager.h"
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Text.hpp>
#include <string>
#include <cmath>
#include <functional>
#include <set>
#include <thread>
#include <array>
#include <iostream>

#include "../VectorMath.h"
#include "../Macros.h"
#include "../KeyDefinitions.h"

GameLogicManager::GameLogicManager(SharedResources& shr) :
	shr_(shr),
	deadEntitiesHandler_(shr_)
{
	shr_.gameMap.loadFromFile("map1.txt");
	createBackground();
	static constexpr int testNum = 200;
	static constexpr float tileSize = TerrainTile::TERRAIN_TILE_SIZE;
	for (int i = 0; i < testNum / 2; ++i)
	{
		sf::Vector2f pos1 = shr_.entityManager.getTerrainTiles().at(i)->getPosition();
		sf::Vector2f pos2 = shr_.entityManager.getTerrainTiles().at(shr_.entityManager.getTerrainTiles().size() - 1 - i)->getPosition();
		createTestUnit(pos1, 1, i < testNum / 4.f, i % 3 == 0);
		createTestUnit(pos2, 2, i < testNum / 4.f, i % 3 == 0);
	}
}

GameLogicManager::~GameLogicManager()
{
}

void GameLogicManager::onMouseButtonPressed(const sf::Event& e)
{
	sf::Vector2f globalPos(
		static_cast<float>(e.mouseButton.x),
		static_cast<float>(e.mouseButton.y)
	);
	switch (e.mouseButton.button)
	{
		case sf::Mouse::Button::Right:
		{
			issueMoveToSelectedUnits(globalPos);
		}
		break;
		case sf::Mouse::Button::Left:
		{
			leftMouseButtonPressedPos_ = globalPos;
			if (shr_.inputManager.isLastPressedKey(KEY_PLACE_BARRACKS))
			{
				auto barracks = new Barracks(shr_);
				barracks->setOwner(playerNumber_);
				barracks->placeOnMap(globalPos);
			}
			if (shr_.inputManager.isLastPressedKey(KEY_PLACE_FACTORY))
			{
				auto factory = new Factory(shr_);
				factory->setOwner(playerNumber_);
				factory->placeOnMap(globalPos);
			}
			if (shr_.inputManager.isLastPressedKey(KEY_PLACE_ATOM))
			{
				auto abb = new AtomBombBuilding(shr_);
				abb->setOwner(playerNumber_);
				abb->placeOnMap(globalPos);
			}
			if (shr_.inputManager.isLastPressedKey(KEY_ATOM_DROP_BOMB))
			{
				for (auto entity : shr_.entityManager.getSelectedEntities())
				{
					auto abb = dynamic_cast<AtomBombBuilding*>(entity);
					if (abb)
						abb->useAtomBomb(globalPos);
				}
			}
		}
		break;
	}
}

void GameLogicManager::onMouseButtonReleased(const sf::Event& e)
{
	sf::Vector2f globalPos(
		static_cast<float>(e.mouseButton.x),
		static_cast<float>(e.mouseButton.y)
	);
	switch (e.mouseButton.button)
	{
	case sf::Mouse::Button::Left:
	{
		selectUnits(globalPos, leftMouseButtonPressedPos_);
	}
	break;
	}
}

void GameLogicManager::onKeyPressed(const sf::Event& e)
{
	if (e.key.code <= sf::Keyboard::Num6 && e.key.code >= sf::Keyboard::Num0)
	{
		playerNumber_ = e.key.code - sf::Keyboard::Num0;
		std::cout << playerNumber_ << "\n";
	}
	for (auto entity : shr_.entityManager.getSelectedEntities())
	{
		auto barracks = dynamic_cast<Barracks*>(entity);
		if (barracks)
		{
			switch (e.key.code)
			{
			case KEY_BARRACKS_TRAIN_INFANTRY:
				barracks->queueInfantry();
				break;
			}
		}
		auto factory = dynamic_cast<Factory*>(entity);
		if (factory)
		{
			switch (e.key.code)
			{
			case KEY_FACTORY_BUILD_TANK:
				factory->queueTank();
				break;
			case KEY_FACTORY_BUILD_PLANE:
				factory->queuePlane();
				break;
			}
		}
	}
}

void GameLogicManager::createBackground()
{
	uint64_t width = shr_.gameMap.getWidth();
	uint64_t height = shr_.gameMap.getHeight();
	for (uint64_t i = 0; i < width * height; ++i)
	{
		TerrainTile* tmpObj = new TerrainTile();
		tmpObj->setTexture(shr_.assets.getTerrainTexture(shr_.gameMap.getTerrainAt(i)));
		sf::FloatRect rect = tmpObj->getLocalBounds();
		tmpObj->setOrigin(rect.width / 2, rect.height / 2);
		tmpObj->setPosition(rect.width * (i % width), rect.height * int(i / height));
		tmpObj->setPassable(shr_.gameMap.isPassableAt(i));
		shr_.entityManager.addEntity(tmpObj);
	}
}



void GameLogicManager::createTestUnit(sf::Vector2f pos, int owner, bool flying, bool tank)
{
	Unit* testUnit;
	if (flying)
	{
		testUnit = createPlane(owner);
	}
	else if (tank)
	{
		testUnit = createTank(owner);
	}
	else
	{
		testUnit = createInfantry(owner);
	}
	testUnit->setPosition(pos);
	shr_.entityManager.addEntity(testUnit);
}

void GameLogicManager::loop()
{
	sf::Time timeStep = loopClock_.restart();
	deadEntitiesHandler_.update();
	for (auto entity : shr_.entityManager.getGameEntities())
	{
		entity->update(timeStep);
		if (entity->isDead())
			deadEntitiesHandler_.addDeadEntity(entity);
	}
}

void GameLogicManager::selectUnits(sf::Vector2f pointA, sf::Vector2f pointB)
{
	sf::FloatRect selectionRect;
	selectionRect.width = std::abs(pointA.x - pointB.x) + 1;
	selectionRect.height = std::abs(pointA.y - pointB.y) + 1;
	selectionRect.left = std::min(pointA.x, pointB.x);
	selectionRect.top = std::min(pointA.y, pointB.y);
	if (!sf::Keyboard::isKeyPressed(KEY_ADD_MORE_TO_SELECTION))
	{
		shr_.entityManager.clearSelectedEntities();
	}
	for (auto entity : shr_.entityManager.getGameEntities())
	{
		auto entityRect = entity->getGlobalBounds();
		if (selectionRect.intersects(entityRect))
		{
			if (!shr_.entityManager.isAlreadySelected(entity))
			{
				shr_.entityManager.addToSelectedEntities(entity);
			}
		}
	}
}

Unit* GameLogicManager::createInfantry(int owner)
{
	Unit* inf = new Infantry(shr_);
	inf->setOwner(owner);
	return inf;
}

Unit* GameLogicManager::createTank(int owner)
{
	Unit* tank = new Tank(shr_);
	tank->setOwner(owner);
	return tank;
}

Unit* GameLogicManager::createPlane(int owner)
{
	Unit* plane = new Plane(shr_);
	plane->setOwner(owner);
	return plane;
}

void GameLogicManager::issueMoveToSelectedUnits(sf::Vector2f goal)
{
	//if shift is pressed queue pathpoints
	bool queuePathpoints = sf::Keyboard::isKeyPressed(KEY_QUEUE_COMMANDS);
	bool attackMove = shr_.inputManager.isLastPressedKey(KEY_ATTACK_MOVE);

	for (auto entity : shr_.entityManager.getSelectedEntities())
	{
		auto building = dynamic_cast<UnitProductionBuilding*>(entity);
		if (building)
		{
			building->setRallyPoint(goal);
		}
	}
	//multithread pathfinding: pack units in chunks and launch a pathfinding thread for them
	//by trial and error it seems that chunk size of 8 gives the best results
	static constexpr size_t UNIT_CHUNK_SIZE = 8;
	std::vector<std::thread> pathfindingThreads;
	pathfindingThreads.reserve(shr_.entityManager.getSelectedEntities().size() / UNIT_CHUNK_SIZE + 1);
	size_t unitI = 0;
	size_t threadI = 0;
	std::array<Unit*, UNIT_CHUNK_SIZE> unitChunk;
	for (auto entity : shr_.entityManager.getSelectedEntities())
	{
		auto unit = dynamic_cast<Unit*>(entity);
		if (unit)
		{
			unitChunk[unitI] = unit;
			if (++unitI == UNIT_CHUNK_SIZE)
			{
				unitI = 0;
				pathfindingThreads.emplace_back(std::thread([=]() {
					for (auto unit : unitChunk)
						unit->issueMove(goal, queuePathpoints, attackMove);
					}));
				++threadI;
			}
		}
	}
	pathfindingThreads.emplace_back(std::thread([=]() {
		for (size_t i = 0; i < unitI; ++i)
			unitChunk[i]->issueMove(goal, queuePathpoints, attackMove);
		}));
	++threadI;
	for (size_t i = 0; i < threadI; ++i)
		pathfindingThreads[i].join();
}
