#pragma once

#include <list>
#include <SFML/Window/Event.hpp>

/// @brief Provides input functionalities that the SFML API doesn't provide.
class InputManager
{
public:

	void onPressedEvent(const sf::Event& e);
	const std::list<sf::Event>& getPressedEventLog() const;
	const sf::Event getLastPressedEvent() const;
	const std::pair<sf::Event, sf::Event> getTwoLastPressedEvents() const;

	bool isLastPressedKey(sf::Keyboard::Key key) const;
	bool isLastPressedKey(sf::Mouse::Button button) const;

private:
	const size_t LOG_SIZE_ = 3;
	std::list<sf::Event> pressedEventsLog_;
};

