#pragma once

#include <vector>
#include <unordered_set>

#include "../GameObjects/WorldEntity.h"
#include "../GameObjects/TerrainTile.h"
#include "../GameObjects/Unit.h"
#include "../GameObjects/Building.h"

/// @brief Entity management class. Stores all entities present in the game world.
/// Also stores entities that are selected by the player.
class EntityManager
{
public:
	/// Add an entity to the collection.
	void addEntity(WorldEntity*);
	void removeAndDeleteEntity(WorldEntity*);

	void addToSelectedEntities(GameEntity* selectedEntity);
	void clearSelectedEntities();
	bool isAlreadySelected(GameEntity* entity);

	const std::vector<TerrainTile*>& getTerrainTiles() const;
	const std::unordered_set<GameEntity*>& getGameEntities() const;
	const std::unordered_set<Unit*>& getUnits() const;
	const std::unordered_set<Building*>& getBuildings() const;
	const std::unordered_set<GameEntity*>& getSelectedEntities() const;

private:
	std::vector<TerrainTile*> terrainTiles_;
	std::unordered_set<GameEntity*> gameEntities_;
	std::unordered_set<GameEntity*> selectedEntities_;
	std::unordered_set<Unit*> units_;
	std::unordered_set<Building*> buildings_;
};

