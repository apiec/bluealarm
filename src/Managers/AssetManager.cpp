#include "AssetManager.h"

#include <fstream>

AssetManager::AssetManager()
{
	size_t terrainTypeCount = static_cast<size_t>(TerrainType::Count);
	backgroundTextures.reserve(terrainTypeCount);

	for (size_t i = 0; i < terrainTypeCount; ++i)
	{
		backgroundTextures.emplace_back();
	}

	loadAssets();
}

const sf::Texture& AssetManager::getTerrainTexture(TerrainType terrainType) const
{
	return backgroundTextures[static_cast<size_t>(terrainType)];
}

void AssetManager::loadTexture(sf::Texture& texture, const std::string& fileName)
{
	if (texture.loadFromFile(fileName))
	{
		
	}
	else
	{
	}
}

void AssetManager::loadAssets()
{
	using TT = TerrainType;
	loadTexture(backgroundTextures[static_cast<size_t>(TT::Grass)], "textures/grassTile.png");
	loadTexture(backgroundTextures[static_cast<size_t>(TT::Rock)], "textures/rockTile.png");
	loadTexture(infantryTexture, "textures/infantry.png");
	loadTexture(planeTexture, "textures/plane.png");
	loadTexture(planeShadowTexture, "textures/planeShadow.png");
	loadTexture(tankTexture, "textures/tank.png");
	loadTexture(barracksTexture, "textures/barracks.png");
	loadTexture(atomBombBuildingTexture, "textures/atomBombBuilding.png");
	loadTexture(factoryTexture, "textures/factory.png");
	if (fonts.spaceage.loadFromFile("fonts/spaceage.ttf"))
	{
	}
}
