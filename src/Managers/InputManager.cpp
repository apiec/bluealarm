#include "InputManager.h"

void InputManager::onPressedEvent(const sf::Event& e)
{
	if (pressedEventsLog_.size() >= LOG_SIZE_)
		pressedEventsLog_.pop_back();

	pressedEventsLog_.emplace_front(e);
}

const std::list<sf::Event>& InputManager::getPressedEventLog() const
{
	return pressedEventsLog_;
}

const sf::Event InputManager::getLastPressedEvent() const
{
	if (!pressedEventsLog_.empty())
		return pressedEventsLog_.front();
	else
		return sf::Event();
}

const std::pair<sf::Event, sf::Event> InputManager::getTwoLastPressedEvents() const
{
	return { pressedEventsLog_.front(), *std::next(pressedEventsLog_.begin()) };
}

bool InputManager::isLastPressedKey(sf::Keyboard::Key key) const
{
	auto lastEvent = getLastPressedEvent();
	return lastEvent.type == sf::Event::KeyPressed
		&& lastEvent.key.code == key;

}

bool InputManager::isLastPressedKey(sf::Mouse::Button button) const
{
	auto lastEvent = getLastPressedEvent();
	return lastEvent.type == sf::Event::MouseButtonPressed
		&& lastEvent.mouseButton.button == button;
}