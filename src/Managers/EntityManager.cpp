#include "EntityManager.h"

void EntityManager::addEntity(WorldEntity* entity)
{
	TerrainTile* terrainTile = dynamic_cast<TerrainTile*>(entity);
	if (terrainTile)
	{
		terrainTiles_.emplace_back(terrainTile);
		return;
	}

	GameEntity* gameEntity = dynamic_cast<GameEntity*>(entity);
	Unit* unit = dynamic_cast<Unit*>(entity);
	Building* building = dynamic_cast<Building*>(entity);

	if (gameEntity)	gameEntities_.insert(gameEntity);
	if (unit)		units_.insert(unit);
	if (building)	buildings_.insert(building);

}

void EntityManager::removeAndDeleteEntity(WorldEntity* entity)
{
	TerrainTile* terrainTile = dynamic_cast<TerrainTile*>(entity);
	if (terrainTile)
	{
		auto it = std::find(terrainTiles_.begin(), terrainTiles_.end(), terrainTile);
		if (it != terrainTiles_.end())
			terrainTiles_.erase(it);
		return;
	}
	GameEntity* gameEntity = dynamic_cast<GameEntity*>(entity);
	Unit* unit = dynamic_cast<Unit*>(entity);
	Building* building = dynamic_cast<Building*>(entity);

	if (gameEntity)
	{
		gameEntities_.erase(gameEntity);
		selectedEntities_.erase(gameEntity);
	}
	if (unit)		units_.erase(unit);
	if (building)	buildings_.erase(building);

	delete entity;
}

void EntityManager::addToSelectedEntities(GameEntity* selectedEntity)
{
	selectedEntities_.insert(selectedEntity);
}

void EntityManager::clearSelectedEntities()
{
	selectedEntities_.clear();
}

bool EntityManager::isAlreadySelected(GameEntity* entity)
{
	return std::find(selectedEntities_.begin(), selectedEntities_.end(), entity) != selectedEntities_.end();
}

const std::unordered_set<GameEntity*>& EntityManager::getGameEntities() const
{
	return gameEntities_;
}

const std::unordered_set<Unit*>& EntityManager::getUnits()	const
{
	return units_;
}

const std::unordered_set<Building*>& EntityManager::getBuildings()	const
{
	return buildings_;
}

const std::vector<TerrainTile*>& EntityManager::getTerrainTiles() const
{
	return terrainTiles_;
}

const std::unordered_set<GameEntity*>& EntityManager::getSelectedEntities() const
{
	return selectedEntities_;
}
