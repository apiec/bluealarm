#pragma once

#include <deque>
#include <mutex>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "../Loopable.h"
#include "../GameObjects/WorldEntity.h"
#include "../GameObjects/Unit.h"
#include "../GameObjects/TerrainTile.h"
#include "../GameObjects/UnitProductionBuilding.h"
#include "AssetManager.h"
#include "../SharedResources.h"


/// @brief Draws all the entities to the window.
class RenderingManager :
	public Loopable
{
public:
	explicit RenderingManager(SharedResources& shr, sf::RenderWindow* window);
	~RenderingManager();
	void loop() override;

	void onMouseMoved(const sf::Event& e);
	void onMouseWheelScrolled(const sf::Event& e);
	void onMouseButtonPressed(const sf::Event& e);
	void onMouseButtonReleased(const sf::Event& e);
	void onResized(const sf::Event& e);

	sf::Vector2f mapPixelToCoords(sf::Vector2i windowCoords);

private:
	const SharedResources& shr_;
	sf::RenderWindow* const window_;
	sf::View windowView_;

	sf::RenderTexture mapTexture_;
	sf::View mapView_;
	sf::Vector2i prevMousePos_;
	sf::Clock fpsClock_;
	sf::Text fpsText_;

	sf::CircleShape selectionIndicator_;
	sf::Vector2f leftMBPressedGlobalPos_;
	bool pressedLMB_;
	sf::VertexArray selectionBox_;
	sf::RectangleShape selectionBoxInside_;
	sf::RectangleShape healthBarHealth_;
	sf::RectangleShape healthBarBackground_;

	sf::RectangleShape productionProgressBar_;
	sf::RectangleShape productionProgressBarBackground_;

	sf::Sprite planeShadowSprite_;

	sf::Sprite barracksPlacingSprite_;
	sf::Sprite unableToPlaceBarracksSprite_;
	sf::Sprite factoryPlacingSprite_;
	sf::Sprite unableToPlaceFactorySprite_;
	sf::Sprite atomPlacingSprite_;
	sf::Sprite unableToPlaceAtomSprite_;
};

