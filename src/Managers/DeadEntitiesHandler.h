#pragma once

#include <map>

#include "../GameObjects/GameEntity.h"
#include "../SharedResources.h"

/// @brief Stores entities for a specified expiration time and then removes them from the game completely.
class DeadEntitiesHandler
{
public:
	explicit DeadEntitiesHandler(SharedResources& shr);
	~DeadEntitiesHandler();
	void addDeadEntity(GameEntity*);
	void update();
private:
	const float EXPIRATION_TIME_IN_SECONDS = 0.5f;
	void checkForEntityExpiration();
	void expireEntity(GameEntity* entity);
	void removeExpiredEntitiesFromGame();

	std::map<GameEntity*, sf::Clock> deadEntities_;
	std::vector<GameEntity*> expiredEntities_;

	SharedResources& shr_;
};

