#pragma once

#include <vector>
#include <unordered_set>

#include "../GameObjects/Unit.h"
#include "../Loopable.h"
#include "../GameObjects/WorldEntity.h"
#include "../GameObjects/TerrainTile.h"
#include "../GameObjects/Building.h"
#include "../GameObjects/UnitProductionBuilding.h"
#include "../GameObjects/SpecializedObjects/AtomBombBuilding.h"
#include "../GameObjects/SpecializedObjects/Barracks.h"
#include "../GameObjects/SpecializedObjects/Factory.h"
#include "../GameObjects/SpecializedObjects/Infantry.h"
#include "../GameObjects/SpecializedObjects/Tank.h"
#include "../GameObjects/SpecializedObjects/Plane.h"
#include "./AssetManager.h"
#include "./InputManager.h"
#include "DeadEntitiesHandler.h"
#include "../SharedResources.h"

/// @brief Manages the logic of the game.
/// The update methods of all entities should only be called in the loop method of this class.
class GameLogicManager :
	public Loopable
{
public:
	explicit GameLogicManager(SharedResources& shr);
	~GameLogicManager();
	void onMouseButtonPressed(const sf::Event& e);
	void onMouseButtonReleased(const sf::Event& e);
	void onKeyPressed(const sf::Event& e);

	void createTestUnit(sf::Vector2f pos, int owner, bool flying, bool tank = false);
	void loop() override;

private:
	SharedResources& shr_;

	DeadEntitiesHandler deadEntitiesHandler_;
	int playerNumber_ = 0;
	sf::Clock loopClock_;
	sf::Vector2f leftMouseButtonPressedPos_;

	void createBackground();
	Unit* createInfantry(int owner);
	Unit* createTank(int owner);
	Unit* createPlane(int owner);

	void selectUnits(sf::Vector2f pointA, sf::Vector2f pointB);

	void issueMoveToSelectedUnits(sf::Vector2f goal);
};
