#include "RenderingManager.h"
 #include "../VectorMath.h"

RenderingManager::RenderingManager(SharedResources& shr, sf::RenderWindow* window) :
	shr_(shr),
	window_(window),
	windowView_(sf::View(window->getView())),
	mapView_(sf::View(window->getView())),
	selectionBox_(sf::LinesStrip, 5),
	pressedLMB_(false)
{
	//fps text
	fpsText_.setFont(shr_.assets.fonts.spaceage);
	fpsText_.setFillColor(sf::Color::Cyan);;
	fpsText_.setCharacterSize(24);
	fpsText_.setOutlineColor(sf::Color::Black);
	fpsText_.setOutlineThickness(4);
	fpsText_.setPosition(0.f, 0.f);

	//selection indicator
	selectionIndicator_.setFillColor(sf::Color::Transparent);
	selectionIndicator_.setOutlineColor(sf::Color::Blue);
	selectionIndicator_.setOutlineThickness(2);
	selectionIndicator_.setScale(1.f, 0.5f);

	//selection box
	sf::Color selectionBoxColor = sf::Color::Blue;
	selectionBox_[0].color = selectionBoxColor;
	selectionBox_[1].color = selectionBoxColor;
	selectionBox_[2].color = selectionBoxColor;
	selectionBox_[3].color = selectionBoxColor;
	selectionBox_[4].color = selectionBoxColor;
	selectionBoxColor.a = 100;
	selectionBoxInside_.setFillColor(selectionBoxColor);
	
	//healthbar
	healthBarHealth_.setFillColor(sf::Color::Green);
	healthBarBackground_.setFillColor(sf::Color::Black);

	//production progress bar
	productionProgressBar_.setFillColor(sf::Color::Cyan);
	productionProgressBarBackground_.setFillColor(sf::Color::Black);

	//plane shadow
	planeShadowSprite_.setTexture(shr_.assets.planeShadowTexture);
	planeShadowSprite_.setOrigin(planeShadowSprite_.getLocalBounds().width / 2.f, planeShadowSprite_.getLocalBounds().height / 2.f);

	//building for placing
	barracksPlacingSprite_.setTexture(shr_.assets.barracksTexture);
	barracksPlacingSprite_.setOrigin(barracksPlacingSprite_.getLocalBounds().width / 2.f, barracksPlacingSprite_.getLocalBounds().height / 2.f);
	barracksPlacingSprite_.setColor(sf::Color(255, 255, 255, 150));

	unableToPlaceBarracksSprite_.setTexture(shr_.assets.barracksTexture);
	unableToPlaceBarracksSprite_.setOrigin(barracksPlacingSprite_.getLocalBounds().width / 2.f, barracksPlacingSprite_.getLocalBounds().height / 2.f);
	unableToPlaceBarracksSprite_.setColor(sf::Color(255, 0, 0, 150));


	factoryPlacingSprite_.setTexture(shr_.assets.factoryTexture);
	factoryPlacingSprite_.setOrigin(factoryPlacingSprite_.getLocalBounds().width / 2.f, barracksPlacingSprite_.getLocalBounds().height / 2.f);
	factoryPlacingSprite_.setColor(sf::Color(255, 255, 255, 150));

	unableToPlaceFactorySprite_.setTexture(shr_.assets.factoryTexture);
	unableToPlaceFactorySprite_.setOrigin(factoryPlacingSprite_.getLocalBounds().width / 2.f, barracksPlacingSprite_.getLocalBounds().height / 2.f);
	unableToPlaceFactorySprite_.setColor(sf::Color(255, 0, 0, 150));

	atomPlacingSprite_.setTexture(shr_.assets.atomBombBuildingTexture);
	atomPlacingSprite_.setOrigin(atomPlacingSprite_.getLocalBounds().width / 2.f, barracksPlacingSprite_.getLocalBounds().height / 2.f);
	atomPlacingSprite_.setColor(sf::Color(255, 255, 255, 150));

	unableToPlaceAtomSprite_.setTexture(shr_.assets.atomBombBuildingTexture);
	unableToPlaceAtomSprite_.setOrigin(atomPlacingSprite_.getLocalBounds().width / 2.f, barracksPlacingSprite_.getLocalBounds().height / 2.f);
	unableToPlaceAtomSprite_.setColor(sf::Color(255, 0, 0, 150));


	sf::Vector2u viewSize(mapView_.getSize());
	mapTexture_.create(viewSize.x, viewSize.y);
}

RenderingManager::~RenderingManager()
{

}

void RenderingManager::loop()
{
	//drawing to map texture
	mapTexture_.clear();
	mapTexture_.setView(mapView_);
	//draw background
	for (auto ob : shr_.entityManager.getTerrainTiles())
	{
		mapTexture_.draw(*ob);
	}

	//draw things specific to selected entities
	for (auto en : shr_.entityManager.getSelectedEntities())
	{
		selectionIndicator_.setPosition(en->getPosition());
		//draw selection indicators
		Unit* un = dynamic_cast<Unit*>(en);
		if (un)
		{
			selectionIndicator_.setScale(1.f, 0.5f);
			selectionIndicator_.setRadius(
				en->getGlobalBounds().width * 0.5f - selectionIndicator_.getOutlineThickness() / 2.f
			);
		}
		else if (en)
		{
			selectionIndicator_.setScale(1.f, 1.f);
			auto newRadius = VectorMath::length(sf::Vector2f{ en->getGlobalBounds().width, en->getGlobalBounds().height }) / 2.f;
			selectionIndicator_.setRadius(newRadius);
		}
		selectionIndicator_.setOrigin(
			0.5f * selectionIndicator_.getLocalBounds().width,
			0.5f * selectionIndicator_.getLocalBounds().height
		);
		mapTexture_.draw(selectionIndicator_);

		//draw the queued path

		if (un)
		{
			auto path = un->getPath();
			sf::Color pathColor;
			if (!un->isIgnoringEnemies())
				pathColor = sf::Color::Red;
			else
				pathColor = sf::Color::Blue;

			if (!path.empty())
			{
				sf::VertexArray lines(sf::LinesStrip, path.size() + 1);
				lines[0].position = un->getPosition();
				lines[0].color = pathColor;
				size_t i = 1;
				for (auto pathpoint : path)
				{
					lines[i].position = static_cast<sf::Vector2f>(pathpoint);
					lines[i].color = pathColor;
					++i;
				}
				mapTexture_.draw(lines);
			}
		}

		auto building = dynamic_cast<UnitProductionBuilding*>(en);
		if (building)
		{
			sf::CircleShape rallyPoint;
			rallyPoint.setFillColor(sf::Color::Blue);
			rallyPoint.setRadius(3);
			rallyPoint.setScale(1.f, 0.5f);
			rallyPoint.setOrigin(rallyPoint.getLocalBounds().width / 2.f, rallyPoint.getLocalBounds().height / 2.f);
			rallyPoint.setPosition(building->getRallyPoint());
			mapTexture_.draw(rallyPoint);

			sf::VertexArray lines(sf::LinesStrip, 2);
			lines[0].position = building->getPosition();
			lines[0].color = sf::Color::Blue;
			lines[1].position = building->getRallyPoint();
			lines[1].color = sf::Color::Blue;
			mapTexture_.draw(lines);
		}
	}

	for (auto ob : shr_.entityManager.getGameEntities())
	{
		mapTexture_.draw(*ob);
		//draw healthbars
		if (!ob->isDead())
		{
			auto obRect = ob->getGlobalBounds();
			healthBarBackground_.setSize(sf::Vector2f(obRect.width + 2.f, 5.f));
			float healthWidth = ob->getCurrentHealth() / ob->getMaxHealth() * obRect.width;
			if (healthWidth < 1.f && healthWidth > 0.f)
				healthWidth = 1.f;
			healthBarHealth_.setSize(sf::Vector2f(healthWidth, 3.f));
			healthBarBackground_.setPosition(obRect.left - 1.f, obRect.top - 7.f);
			healthBarHealth_.setPosition(healthBarBackground_.getPosition() + sf::Vector2f{ 1.f, 1.f });//obRect.left, obRect.top - 6.f);
			mapTexture_.draw(healthBarBackground_);
			mapTexture_.draw(healthBarHealth_);
			auto building = dynamic_cast<UnitProductionBuilding*>(ob);
			if (building)
			{
				productionProgressBarBackground_.setSize(sf::Vector2f(obRect.width + 2.f, 5.f));
				float productionBarWidth = building->getProductionProgress() * obRect.width;
				productionProgressBar_.setSize(sf::Vector2f(productionBarWidth, 3.f));
				productionProgressBarBackground_.setPosition(obRect.left - 1.f, obRect.top - 15.f);
				productionProgressBar_.setPosition(productionProgressBarBackground_.getPosition() + sf::Vector2f{ 1.f, 1.f });
				mapTexture_.draw(productionProgressBarBackground_);
				mapTexture_.draw(productionProgressBar_);
			}
		}
	}


	//redraw flying units so they are over ground entities
	for (auto ob : shr_.entityManager.getUnits())
	{
		if (ob->isFlying())
		{
			//draw entities
			mapTexture_.draw(*ob);
			planeShadowSprite_.setScale(1.1f * ob->getScale());
			planeShadowSprite_.setPosition(ob->getPosition());
			mapTexture_.draw(planeShadowSprite_);
			//if entity's alive draw its healthbar
			if (!ob->isDead())
			{
				auto obRect = ob->getGlobalBounds();
				healthBarBackground_.setSize(sf::Vector2f(obRect.width + 2.f, 5.f));
				float healthWidth = ob->getCurrentHealth() / ob->getMaxHealth() * obRect.width;
				if (healthWidth < 1.f && healthWidth > 0.f) healthWidth = 1.f;
				healthBarHealth_.setSize(sf::Vector2f(healthWidth, 3.f));
				healthBarBackground_.setPosition(obRect.left - 1.f, obRect.top - 7.f);
				healthBarHealth_.setPosition(obRect.left, obRect.top - 6.f);
				mapTexture_.draw(healthBarBackground_);
				mapTexture_.draw(healthBarHealth_);
			}
		}
	}
	//draw selection box
	if (pressedLMB_)
	{
		auto mouseWindowPos = sf::Mouse::getPosition(*window_);
		auto mouseGlobalPos = mapTexture_.mapPixelToCoords(mouseWindowPos);
		selectionBox_[0].position = sf::Vector2f(leftMBPressedGlobalPos_.x, leftMBPressedGlobalPos_.y);
		selectionBox_[1].position = sf::Vector2f(leftMBPressedGlobalPos_.x, mouseGlobalPos.y);
		selectionBox_[2].position = sf::Vector2f(mouseGlobalPos.x, mouseGlobalPos.y);
		selectionBox_[3].position = sf::Vector2f(mouseGlobalPos.x, leftMBPressedGlobalPos_.y);
		selectionBox_[4].position = selectionBox_[0].position;

		sf::Vector2f sz;
		sz.x = std::abs(mouseGlobalPos.x - leftMBPressedGlobalPos_.x);
		sz.y = std::abs(mouseGlobalPos.y - leftMBPressedGlobalPos_.y);
		sf::Vector2f pos;
		pos.x = std::min(mouseGlobalPos.x, leftMBPressedGlobalPos_.x);
		pos.y = std::min(mouseGlobalPos.y, leftMBPressedGlobalPos_.y);

		selectionBoxInside_.setSize(sz);
		selectionBoxInside_.setPosition(pos);

		mapTexture_.draw(selectionBox_);
		mapTexture_.draw(selectionBoxInside_);
	}

	//if going to place a building show an outline of it
	bool placingBarracks = shr_.inputManager.isLastPressedKey(sf::Keyboard::B);
	bool placingFactory = shr_.inputManager.isLastPressedKey(sf::Keyboard::F);
	bool placingAtom = shr_.inputManager.isLastPressedKey(sf::Keyboard::V);


	if (placingBarracks || placingFactory || placingAtom)
	{
		auto mousePos = sf::Mouse::getPosition(*window_);
		auto worldPos = sf::Vector2i(mapPixelToCoords(mousePos));
		const auto tts = TerrainTile::TERRAIN_TILE_SIZE;
		sf::Vector2f buildingPos{ static_cast<float>(worldPos.x - worldPos.x % tts + tts / 2),
								  static_cast<float>(worldPos.y - worldPos.y % tts + tts / 2) };

		if (placingBarracks)
		{
			barracksPlacingSprite_.setPosition(buildingPos);
			mapTexture_.draw(barracksPlacingSprite_);
		}
		else if (placingFactory)
		{
			factoryPlacingSprite_.setPosition(buildingPos);
			mapTexture_.draw(factoryPlacingSprite_);
		}
		else if (placingAtom)
		{
			atomPlacingSprite_.setPosition(buildingPos);
			mapTexture_.draw(atomPlacingSprite_);
		}
	}


	mapTexture_.display();

//drawing to the window
	window_->clear();
	window_->setView(windowView_);

	window_->draw(sf::Sprite(mapTexture_.getTexture()));

	//draw fps
	sf::Time frameTime = fpsClock_.restart();
	float fps = 1.f / frameTime.asSeconds();
	fpsText_.setString(std::to_string((int)fps));

	window_->draw(fpsText_);

	window_->display();
}

void RenderingManager::onMouseMoved(const sf::Event& e)
{
	if (sf::Mouse::isButtonPressed(sf::Mouse::Middle))
	{
		mapView_.move(
			static_cast<float>(prevMousePos_.x - e.mouseMove.x),
			static_cast<float>(prevMousePos_.y - e.mouseMove.y)
			);
	}
	prevMousePos_ = sf::Vector2i(e.mouseMove.x, e.mouseMove.y);
}

void RenderingManager::onMouseWheelScrolled(const sf::Event& e)
{
	mapView_.zoom(1.f - e.mouseWheelScroll.delta * 0.1f);
}

void RenderingManager::onMouseButtonPressed(const sf::Event& e)
{
	sf::Vector2i windowPos(e.mouseButton.x, e.mouseButton.y);

	switch (e.mouseButton.button)
	{
	case sf::Mouse::Button::Left:
	{
		pressedLMB_ = true;
		leftMBPressedGlobalPos_ = mapTexture_.mapPixelToCoords(windowPos);
	}
	break;
	}
}

void RenderingManager::onMouseButtonReleased(const sf::Event& e)
{
	switch (e.mouseButton.button)
	{
	case sf::Mouse::Button::Left:
	{
		pressedLMB_ = false;
	}
	}
}

void RenderingManager::onResized(const sf::Event& e)
{
	sf::Vector2u newSizeu(e.size.width, e.size.height);
	sf::Vector2f newSizef(newSizeu);
	mapView_.setSize(newSizef);
	mapTexture_.create(newSizeu.x, newSizeu.y);
	mapTexture_.setView(mapView_);
	windowView_.setSize(newSizef);
	windowView_.setCenter(newSizef.x / 2, newSizef.y / 2);
}

sf::Vector2f RenderingManager::mapPixelToCoords(sf::Vector2i pixelCoords)
{
	return mapTexture_.mapPixelToCoords(pixelCoords);
}