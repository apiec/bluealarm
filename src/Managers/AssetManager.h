#pragma once

#include <vector>
#include<SFML/Graphics.hpp>

#include "../GameObjects/GameMap.h"

/// @brief Holds assets used in the game (eg. textures, fonts etc.)
class AssetManager
{
public:
	AssetManager();
	const sf::Texture& getTerrainTexture(TerrainType terrainType) const;
	sf::Texture infantryTexture;
	sf::Texture planeTexture;
	sf::Texture planeShadowTexture;
	sf::Texture tankTexture;
	sf::Texture barracksTexture;
	sf::Texture atomBombBuildingTexture;
	sf::Texture factoryTexture;
	struct {
		sf::Font spaceage;
	} fonts;

private:
	void loadTexture(sf::Texture& texture, const std::string& fileName);
	void loadAssets();
	std::vector<sf::Texture> backgroundTextures;
	
};