#pragma once

#include <vector>
#include <unordered_set>
#include <SFML/Graphics/RenderWindow.hpp>

#include "EventDispatcher.h"
#include "Managers/AssetManager.h"
#include "Managers/InputManager.h"
#include "Managers/GameLogicManager.h"
#include "Managers/RenderingManager.h"
#include "GameObjects/WorldEntity.h"
#include "GameObjects/TerrainTile.h"

#include "SharedResources.h"

/// @brief Encompasses the entire game.
class App
{
public:
	App();
	void start();

private:

	void mainLoop();

	sf::RenderWindow window_;

	SharedResources shr_;

	GameLogicManager objectManager_;
	RenderingManager renderingManager_;
	EventDispatcher eventDispatcher_;
};