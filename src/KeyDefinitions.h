#pragma once

#include <SFML/Window/Keyboard.hpp>

constexpr sf::Keyboard::Key KEY_ATTACK_MOVE = sf::Keyboard::A;
constexpr sf::Keyboard::Key KEY_ADD_MORE_TO_SELECTION = sf::Keyboard::LShift;
constexpr sf::Keyboard::Key KEY_QUEUE_COMMANDS = sf::Keyboard::LShift;

constexpr sf::Keyboard::Key KEY_PLACE_BARRACKS = sf::Keyboard::B;
constexpr sf::Keyboard::Key KEY_PLACE_FACTORY = sf::Keyboard::F;
constexpr sf::Keyboard::Key KEY_PLACE_ATOM = sf::Keyboard::V;

constexpr sf::Keyboard::Key KEY_BARRACKS_TRAIN_INFANTRY = sf::Keyboard::Q;

constexpr sf::Keyboard::Key KEY_FACTORY_BUILD_TANK = sf::Keyboard::Q;
constexpr sf::Keyboard::Key KEY_FACTORY_BUILD_PLANE = sf::Keyboard::W;

constexpr sf::Keyboard::Key KEY_ATOM_DROP_BOMB = sf::Keyboard::Q;