#pragma once

#include <list>
#include <vector>
#include <unordered_set>

#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>

#include "WorldEntity.h"
#include "GameEntity.h"
#include "GameMap.h"

struct SharedResources;

/// @brief Collection of Unit states.
struct UnitState :
	public GameEntityState
{
	static constexpr int Moving		= 10;
	static constexpr int Attacking	= 12;
};


 /// @brief A unit can move around the map and attack units with different owners.
class Unit :
	public GameEntity
{
public:
	Unit(SharedResources& shr);
	
	/*
	 * @brief Called every game loop.
	 * Every unit does the following actions every loop:
	 *   -If it's been issued a move command then it moves towards the next pathpoint
	 *   -If it's in other states it:
	 *     -Searches for enemies in its attack range.
	 *     -If it finds one then it attacks the one closest to it.
	 *     -If not then:
	 *       -If it's idle it remains idle.
	 *		 -If it's attackmoving it moves towards the next pathpoint.
	 */
	virtual void update(const sf::Time& timeStep) override;

	/*
	 * @brief Issue a move to a point on the map.
	 * @param[in] goal Position that a unit is issued to reach.
	 * @param[in] queuePathpoints Should the unit end its current move command before following this one.
	 * @param[in] attackMove Is the command a normal move or an attack move command.
	 */
	void issueMove(sf::Vector2f goal, bool queuePathpoints, bool attackMove);

	void setMoveSpeed(float);
	void setAttackValue(float);
	void setAttackSpeed(float);
	void setAttackRange(float);
	void setFlying(bool flying);

	float getMoveSpeed() const;
	float getAttackValue() const;
	float getAttackSpeed() const;
	float getAttackRange() const;

	bool isIgnoringEnemies() const;
	bool isFlying() const;
	

	/// Get the unit's next pathpoint.
	sf::Vector2f getNextPathpoint() const;
	/// Get the list of pathpoints the unit is issued to reach.
	const std::list<sf::Vector2f>& getPath() const;

protected:
	/// Calculates the direction of the next step of the unit.
	virtual sf::Vector2f getMovementDirection() const;
	/// Make a step towards the next pathpoint.
	virtual void takeStep(const sf::Time& timeStep);
	/// Attack a GameEntity.
	virtual void attack(GameEntity* entity);

	/// Add a pathpoint to the unit's path.
	void addPathpoint(sf::Vector2f pathPoint);
	/// Clear the unit's path.
	void clearPath();

	/// Set the unit's sprite's origin point to be in the middle of the sprite.
	void recalculateOrigin();
private:
	/// Determines whether the unit is considered as flying or not.
	bool flying_;
	/// Speed at which the unit moves in the world.
	float moveSpeed_;
	/// The amount of damage the unit does with its attack.
	float attackValue_;
	/// The time the unit takes between attacks in seconds.
	float attackSpeed_;
	/// The range of the unit's attack. 
	float attackRange_;
	/// Should the unit ignore enemies or attack them.
	bool ignoreEnemies_ = false;

	/// Resolve collisions with other WorldEntities.
	void resolveCollisions(const sf::Time& timeStep);
	/// Resolve a collision where no overlap is possible.
	void resolveHardCollision(WorldEntity* entity);
	/// Resolve a collision with a unit.
	void resolveUnitCollision(Unit* unit, const sf::Time& timeStep);

	/// The target of the unit.
	GameEntity* target_ = nullptr;
	/// Search for the nearest enemy that is in Unit's attackRange.
	GameEntity* findNearestEnemyInRange();

	/// The list of pathpoints the unit is issued to reach.
	std::list<sf::Vector2f> path_;

	/// Keeps track of how much time has passed since the unit's last attack.
	sf::Clock attackClock_;
};

