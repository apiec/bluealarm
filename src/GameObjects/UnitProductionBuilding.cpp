#include "UnitProductionBuilding.h"
#include "../SharedResources.h"

UnitProductionBuilding::UnitProductionBuilding(SharedResources& shr) :
	Building(shr)
{
}


void UnitProductionBuilding::update(const sf::Time& timeStep)
{
	GameEntity::update(timeStep);

	auto unit = productionQueue_.update();
	if (unit)
		spawnUnit(unit);
}

void UnitProductionBuilding::placeOnMap(sf::Vector2f pos)
{
	Building::placeOnMap(pos);
	auto gb = this->getGlobalBounds();
	spawnPoint_ = sf::Vector2f{ gb.left + gb.width / 2.f, gb.top + gb.height + 15.f };
	rallyPoint_ = spawnPoint_ + sf::Vector2f{ 10.f, 10.f };
}

sf::Vector2f UnitProductionBuilding::getSpawnPoint() const
{
	return spawnPoint_;
}

void UnitProductionBuilding::setRallyPoint(sf::Vector2f rallyPoint)
{
	rallyPoint_ = rallyPoint;
}
sf::Vector2f UnitProductionBuilding::getRallyPoint() const
{
	return rallyPoint_;
}

float UnitProductionBuilding::getProductionProgress() const
{
	return productionQueue_.getProductionProgress();
}

void UnitProductionBuilding::spawnUnit(Unit* unit)
{
	unit->setPosition(spawnPoint_);
	unit->issueMove(rallyPoint_, false, false);
	shr_.entityManager.addEntity(unit);
}
