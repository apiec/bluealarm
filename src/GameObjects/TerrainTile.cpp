#include "TerrainTile.h"

TerrainTile::TerrainTile() :
	passable_(true)
{
}

bool TerrainTile::isPassable() const
{
	return passable_;
}
void TerrainTile::setPassable(bool passable)
{
	passable_ = passable;
}
