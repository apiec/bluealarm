#include "GameMap.h"
#include <fstream>
#include "../VectorMath.h"

GameMap::GameMap() :
	width_(0),
	height_(0),
	tileSize_(static_cast<float>(TerrainTile::TERRAIN_TILE_SIZE))
{
}

GameMap::~GameMap()
{
	for (auto pm : passableMaps_)
		delete pm;
}

uint64_t GameMap::getWidth() const
{
	return width_;
}

uint64_t GameMap::getHeight() const
{
	return height_;
}

TerrainType GameMap::getTerrainAt(uint64_t pos) const
{
	return terrainData_[pos];
}

TerrainType GameMap::getTerrainAt(uint64_t x, uint64_t y) const
{
	return terrainData_[x + y * width_];
}

bool GameMap::isPassableAt(uint64_t pos, uint64_t thickenWidth) const
{
	if (pos < 0 || pos > width_ * height_) return false;
	while (passableMaps_.size() <= thickenWidth)
	{
		passableMaps_.emplace_back(nullptr);
	}	
	if (passableMaps_[thickenWidth] == nullptr)
	{
		passableMaps_[thickenWidth] = new std::vector<bool>;
 		recalculatePassabilityMap(thickenWidth);
	}
	return passableMaps_[thickenWidth]->at(pos);
}

void GameMap::recalculatePassabilityMaps() const
{
	for (size_t i = 0; i < passableMaps_.size(); ++i)
		recalculatePassabilityMap(i);
}

void GameMap::recalculatePassabilityMap(size_t pos) const
{
	passableMaps_[pos]->clear();
	for (size_t i = 0; i < width_ * height_; ++i)
	{
		passableMaps_[pos]->emplace_back(true);
	}
	for (size_t x = pos; x < width_ - pos; ++x)
	{
		for (size_t y = pos; y < height_ - pos; ++y)
		{
			for (size_t xNeighbor = x - pos; xNeighbor <= x + pos; ++xNeighbor)
			{
				if (passableMap_[xNeighbor + y * width_] == false)
				{
					passableMaps_[pos]->at(x + y * width_) = false;
					break;
				}
			}
		}
	}
}

bool GameMap::isPassableAt(uint64_t x, uint64_t y, uint64_t width) const
{
	return isPassableAt(x + y * width_, width);
}

void GameMap::setPassability(uint64_t pos, bool passable)
{
	passableMap_[pos] = passable;
	recalculatePassabilityMaps();
}

void GameMap::setPassability(uint64_t x, uint64_t y, bool passable)
{
	setPassability(x + y * width_, passable);
}

void GameMap::loadFromFile(const std::string& fileName)
{
	std::ifstream inFile;
	inFile.open(fileName);
	if (!inFile)
	{
		std::exit(1);
	}
	inFile >> width_;
	inFile >> height_;

	terrainData_.clear();
	terrainData_.reserve(width_ * height_);
	for (uint64_t i = 0; i < width_ * height_; ++i)
	{
		if (inFile)
		{
			uint64_t tmp;
			inFile >> tmp;
			TerrainType terrain = static_cast<TerrainType>(tmp);
			terrainData_.emplace_back(terrain);
			bool isPassable = (
				std::find(impassables_.begin(), impassables_.end(), terrain) == impassables_.end()
				);
			passableMap_.emplace_back(isPassable);
		}
		else
		{
			std::exit(1);
		}
	}
}

sf::Vector2f GameMap::gridToWorldCoord(sf::Vector2i gridCoord) const
{
	return  tileSize_ * sf::Vector2f(gridCoord);
}

sf::Vector2i GameMap::worldToGridCoord(sf::Vector2f worldCoord) const
{
	return  sf::Vector2i(
		static_cast<int>(std::roundf(worldCoord.x / tileSize_)),
		static_cast<int>(std::roundf(worldCoord.y / tileSize_))
	);
}

float GameMap::heuristic(sf::Vector2i pos, sf::Vector2i goal) const
{
	return static_cast<float>(std::abs(goal.x - pos.x) + std::abs(goal.y - pos.y));
}

bool GameMap::findPath(sf::Vector2f start, sf::Vector2f goal, std::deque<sf::Vector2f>& path, uint64_t unitWidth) const
{
	sf::Vector2i gridStart = worldToGridCoord(start);
	sf::Vector2i gridGoal = worldToGridCoord(goal);
	if (gridGoal.x < 0 || gridGoal.x >= width_  ||
		gridGoal.y < 0 || gridGoal.y >= height_ ||
		!this->isPassableAt(gridGoal.x, gridGoal.y, unitWidth))
	{
		return false;
	}
	std::vector<PathfindingTile> grid;
	grid.reserve(width_ * height_);
	for (size_t i = 0; i < width_ * height_; ++i)
	{
		grid.emplace_back();
		grid[i].pos.x = static_cast<int>(i % width_);
		grid[i].pos.y = static_cast<int>(i / width_);
	}
	size_t startIndex = gridStart.x + gridStart.y * width_;
	grid[startIndex].fScore = heuristic(gridStart, gridGoal);
	grid[startIndex].gScore = 0;
	grid[startIndex].isInOpenSet = true;

	bool openSetEmpty = false;
	while (!openSetEmpty)
	{
		openSetEmpty = true;
		PathfindingTile current;
		for (auto g : grid)
		{
			if (g.isInOpenSet)
			{
				openSetEmpty = false;
				if (g.fScore < current.fScore)
					current = g;
			}
		}
		if (current.pos == gridGoal)	//reconstruct the path
		{
			while (current.cameFrom != sf::Vector2i(-1, -1))
			{
				current = grid[current.cameFrom.x + current.cameFrom.y * width_];
				sf::Vector2f currentWorldPos = gridToWorldCoord(current.pos);
				path.emplace_front(currentWorldPos);
			}
			path.emplace_front(start);
			path.emplace_back(goal);
			simplifyPath(path, unitWidth);
			return true;
		}

		grid[current.pos.x + current.pos.y * width_].isInOpenSet = false;

		std::vector<sf::Vector2i> neighbors = {
			current.pos + sf::Vector2i(-1, 0),
			current.pos + sf::Vector2i(-1, -1),
			current.pos + sf::Vector2i(0, -1),
			current.pos + sf::Vector2i(1, -1),
			current.pos + sf::Vector2i(1, 0),
			current.pos + sf::Vector2i(1, 1),
			current.pos + sf::Vector2i(0, 1),
			current.pos + sf::Vector2i(-1, 1),
		};
		for (auto n : neighbors)
		{
			if (n.x < 0 || n.x >= width_  ||
				n.y < 0 || n.y >= height_ ||
				!this->isPassableAt(n.x, n.y, unitWidth)
				) continue;
			float d = (std::abs(current.pos.x - n.x) + std::abs(current.pos.y - n.y)) > 1 ? 1.41f : 1.f;
			float tentativeGScore = current.gScore + d;
			size_t neighborIndex = n.x + n.y * width_;
			if (tentativeGScore < grid[neighborIndex].gScore)
			{
				grid[neighborIndex].cameFrom = current.pos;
				grid[neighborIndex].gScore = tentativeGScore;
				grid[neighborIndex].fScore = tentativeGScore + heuristic(n, gridGoal);
				grid[neighborIndex].isInOpenSet = true;
			}
		}
	}
	return false;
}

void GameMap::simplifyPath(std::deque<sf::Vector2f>& path, uint64_t unitWidth) const
{
	if (path.size() < 3) return;
	static constexpr float stepLength = 1.f;
	auto itA = path.begin();
	auto itB = path.end() - 1;
	size_t i = 0;
	while (itB != itA)
	{
		sf::Vector2f rayPos = *itA;
		sf::Vector2f rayGoal = *itB;
		sf::Vector2f rayDir = VectorMath::normalize(rayGoal - rayPos);
		bool hitImpassable = false;
		while (VectorMath::lengthSquared(rayGoal - rayPos) > stepLength * stepLength)
		{
			rayPos += rayDir * stepLength;
			auto gridRayPos = worldToGridCoord(rayPos);
			if (!this->isPassableAt(gridRayPos.x, gridRayPos.y, unitWidth))
			{
				hitImpassable = true;
				break;
			}
		}
		if (hitImpassable)
		{
			--itB;
		}
		else
		{
			++i;
			path.erase(++itA, itB);
			itA = path.begin() + i;
			itB = path.end() - 1;
		}
	}
}