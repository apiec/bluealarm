#pragma once

#include <vector>
#include "WorldEntity.h"
#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>


struct SharedResources;

/// @brief Collection of states that an entity can be in.
/// This is done not with an enum to make inheritance possible.
/// class inheriting from GameEntity may need to have more states.
struct GameEntityState
{
	static constexpr int Dead = 0;
	static constexpr int Idle = 1;
};


/// @brief A Game Entity is an entity that can interact with other Game Entities.
/// It is characterised by having an owner, having health and having a state.
class GameEntity :
	public WorldEntity
{
public:
	GameEntity(SharedResources& shr);
	virtual ~GameEntity();

	int getState() const;

	void setOwner(int owner);
	int getOwner() const;

	void setMaxHealth(float maxHealth);
	float getMaxHealth() const;

	/// Set currentHealth to a value. If value > maxHealth set maxHealth.
	void setCurrentHealth(float currentHealth);
	/// Change currentHealth by param.
	void changeCurrentHealthBy(float changeValue);
	float getCurrentHealth() const;
	/// Updates the state of the entity. Needs to be called on every game loop.
	/// If overriding this method this should be called in the overriden method.
	/// The update method encompasses all behavior of an entity that happens every game loop.
	virtual void update(const sf::Time& timeStep);

	/// Change the state of the entity to Dead.
	void kill();
	/// Check if the state of the entity is Dead.
	bool isDead() const;
	/// Called whenever the enity should get damaged.
	virtual void takeDamage(float attackValue);

protected:
	/// Set the state of the entity. 
	void setState(int state);
	/// Used to check if the entity should go to the Dead state.
	virtual bool deathCondition() const;
	/// Reference to a SharedResources instance used in the game.
	SharedResources& shr_;

private:
	/// The state of the entity.
	int state_;
	/// The number identifying the player owning the entity.
	int owner_;

	/// Maximum health points of the entity.
	float maxHealth_;
	/// The current health points of the entity.
	float currentHealth_;

	/// Used for GUI. The entity changes its color depending on who owns it.
	const static inline std::vector<sf::Color> teamColors_ = {
		sf::Color::White,
		sf::Color(255, 200, 200),
		sf::Color(200, 200, 255),
		sf::Color(200, 255, 200),
		sf::Color(255, 255, 145),
		sf::Color(255, 145, 255),
		sf::Color(145, 255, 255)
	};
};

