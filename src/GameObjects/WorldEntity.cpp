#include "WorldEntity.h"
#include "../VectorMath.h"
#include <cmath>

float WorldEntity::distanceToSquared(const WorldEntity& object) const
{
	return VectorMath::lengthSquared(this->getPosition() - object.getPosition());
}

float WorldEntity::distanceTo(const WorldEntity& object) const
{
	return VectorMath::length(this->getPosition() - object.getPosition());
}
