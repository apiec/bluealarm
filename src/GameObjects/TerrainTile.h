#pragma once
#include "WorldEntity.h"

#include <functional>


/// @brief A tile representing a piece of terrain from the Game Map.
/// A terrain tile can be passable or not.
class TerrainTile : public WorldEntity
{
public:

	TerrainTile();
	
	/// The size (in pixels) that all terrain tiles must be.
	static constexpr uint64_t TERRAIN_TILE_SIZE = 64;
	/// Check whether a tile is passable or not.
	bool isPassable() const;
	/// Set the passabilty of a tile.
	void setPassable(bool passable);

private:
	bool passable_;
};

