#include "Unit.h"
#include <cmath>
#include "../SharedResources.h"
#include "../VectorMath.h"


Unit::Unit(SharedResources& shr) :
	GameEntity(shr),
	flying_(false),
	moveSpeed_(100.f),
	attackValue_(5),
	attackSpeed_(0.1f),
	attackRange_(200)
{
}

void Unit::update(const sf::Time& timeStep)
{
	GameEntity::update(timeStep);

	if (this->isDead()) return;


	//if there's no target set or the target got out of range search for a new target
	if (!target_ || this->distanceToSquared(*target_) > attackRange_ * attackRange_ || target_->isDead())
	{
		target_ = findNearestEnemyInRange();
	}

	switch (this->getState())
	{
	case UnitState::Idle:
	{
		if (target_ && !ignoreEnemies_)
		{
			this->setState(UnitState::Attacking);
		}
		else if (!path_.empty())
		{
			this->setState(UnitState::Moving);
		}
	}
	break;
	case UnitState::Moving:
	{
		takeStep(timeStep);
		if (path_.empty())
		{
			ignoreEnemies_ = false;
			this->setState(UnitState::Idle);
		}
		else if (target_ && !ignoreEnemies_)
		{
			this->setState(UnitState::Attacking);
		}
	}
	break;
	case UnitState::Attacking:
	{
		if (ignoreEnemies_ || !target_)
		{
			this->setState(UnitState::Idle);
		}
		else if (attackClock_.getElapsedTime().asSeconds() > attackSpeed_)
		{
			attackClock_.restart();
			attack(target_);
		}
	}
	break;
	}
	resolveCollisions(timeStep);
}

void Unit::issueMove(sf::Vector2f goal, bool queuePathpoints, bool attackMove)
{
	ignoreEnemies_ = !attackMove;
	sf::Vector2f startPos;
	if (!queuePathpoints || path_.empty())
		startPos = this->getPosition();
	else
		startPos = path_.back();
	if (this->isFlying())
	{
		if (!queuePathpoints) this->clearPath();
		this->addPathpoint(goal);
	}
	else
	{
		std::deque<sf::Vector2f> path;
		uint64_t w = static_cast<uint64_t>(this->getGlobalBounds().width);
		uint64_t unitWidth = w / TerrainTile::TERRAIN_TILE_SIZE;
		if (shr_.gameMap.findPath(startPos, goal, path, unitWidth))
		{
			if (!queuePathpoints) this->clearPath();

			for (auto pp : path)
				this->addPathpoint(pp);
		}
	}
}

void Unit::recalculateOrigin()
{
	if (!this->isFlying())
	{
		this->setOrigin(this->getLocalBounds().width / 2.f, this->getLocalBounds().height);
	}
	else
	{
		this->setOrigin(this->getLocalBounds().width / 2.f, this->getLocalBounds().height + 2 * 32.f / this->getScale().y);
	}
}

void Unit::setMoveSpeed(float moveSpeed)
{
	moveSpeed_ = moveSpeed;
}
void Unit::setAttackValue(float attackValue)
{
	attackValue_ = attackValue;
}
void Unit::setAttackSpeed(float attackSpeed)
{
	attackSpeed_ = attackSpeed;
}
void Unit::setAttackRange(float attackRange)
{
	attackRange_ = attackRange;
}
void Unit::setFlying(bool flying)
{
	flying_ = flying;
}
float Unit::getMoveSpeed() const
{
	return moveSpeed_;
}
float Unit::getAttackValue() const
{
	return attackValue_;
}
float Unit::getAttackSpeed() const
{
	return attackSpeed_;
}
float Unit::getAttackRange() const
{
	return attackRange_;
}
bool Unit::isFlying() const
{
	return flying_;
}
bool Unit::isIgnoringEnemies() const
{
	return ignoreEnemies_;
}


void Unit::resolveCollisions(const sf::Time& timeStep)
{
	for (auto unit : shr_.entityManager.getUnits())
	{
		if (this != unit)
		{
			//flying units can't collide with ground units and vice versa
			if (unit->isFlying() == this->isFlying())
			{
				//if unit's friendly use unit collision
				if (this->getOwner() == unit->getOwner())
				{
					resolveUnitCollision(unit, timeStep);
				}
				else //if unit's not friendly use hard collision
				{
					resolveHardCollision(unit);
				}
			}
		}
	}
	if (!this->isFlying())
	{
		auto rect = this->getGlobalBounds();
		auto prevScale = this->getScale();
		float newYScale = prevScale.y * rect.width / 4.f / rect.height;
		this->setScale(prevScale.x, newYScale);
		for (auto terrainTile : shr_.entityManager.getTerrainTiles())
		{
			if (!terrainTile->isPassable())
			{
				resolveHardCollision(terrainTile);
			}
		}
		for (auto building : shr_.entityManager.getBuildings())
		{
			resolveHardCollision(building);
		}
		this->setScale(prevScale);
	}
}

void Unit::resolveHardCollision(WorldEntity* entity)
{
	if (!this->getGlobalBounds().intersects(entity->getGlobalBounds()))
		return;

	float leftA = this->getGlobalBounds().left;
	float rightA = leftA + this->getGlobalBounds().width;
	float topA = this->getGlobalBounds().top;
	float botA = topA + this->getGlobalBounds().height;

	float leftB = entity->getGlobalBounds().left;
	float rightB = leftB + entity->getGlobalBounds().width;
	float topB = entity->getGlobalBounds().top;
	float botB = topB + entity->getGlobalBounds().height;

	float overlapLeft = std::abs(rightA - leftB);
	float overlapRight = std::abs(rightB - leftA);

	float overlapX = overlapLeft < overlapRight ? -overlapLeft : overlapRight;

	float overlapTop = std::abs(botA - topB);

	float overlapBot = std::abs(botB - topA);

	float overlapY = overlapTop < overlapBot ? -overlapTop : overlapBot;

	if (std::abs(overlapX) < std::abs(overlapY))
	{
		this->move(overlapX, 0.f);
	}
	else
	{
		this->move(0.f, overlapY);
	}
}

void Unit::resolveUnitCollision(Unit* unit, const sf::Time& timeStep)
{
	auto aRect = this->getGlobalBounds();
	auto bRect = unit->getGlobalBounds();

	if (!aRect.intersects(bRect)) return;

	sf::Vector2f aCenter(aRect.left + aRect.width / 2.f, aRect.top + aRect.height / 2.f);
	sf::Vector2f bCenter(bRect.left + bRect.width / 2.f, bRect.top + bRect.height / 2.f);
	sf::Vector2f centerDelta = aCenter - bCenter;
	sf::Vector2f moveDir;
	static constexpr float eps = 10 * std::numeric_limits<float>::epsilon();
	if (centerDelta.x < eps && centerDelta.y < eps)
	{
		float rnd = (std::rand() - 0.5f) * eps / 5.f;
		moveDir = { eps + rnd, eps + rnd };
		moveDir.x *= std::signbit(centerDelta.x) ? -1 : 1;
		moveDir.y *= std::signbit(centerDelta.y) ? -1 : 1;
	}
	else
	{
		moveDir = VectorMath::normalize(aCenter - bCenter);
	}
	moveDir *= timeStep.asSeconds();

	bool isAIdle = this->getState() == UnitState::Idle;
	bool isBIdle = unit->getState() == UnitState::Idle;
#if 1
	//units stack when moving
	if (isAIdle && isBIdle)
	{
		this->move(this->getMoveSpeed() * moveDir / 4.f);
		unit->move(-unit->getMoveSpeed() * moveDir / 4.f);
	}
	else if (isAIdle)
	{
		this->move(this->getMoveSpeed() * moveDir / 2.f);
	}
	else if (isBIdle)
	{
		unit->move(-unit->getMoveSpeed() * moveDir / 2.f);
	}
#else
	//units don't stack when moving.
	//causes weird behavior when a group of units is rallied to a single point
	//TODO: use this but change the way move commands are issued
	//      so the units don't get rallied to a single point
	//      or change the way units decide if they've arrived
	//      (some kind of a message system, units could announce
	//      that they've arrived and that could trigger a stop for other units)
	if (isAIdle && !isBIdle)
	{
		this->move(this->moveSpeed_ * moveDir / 2.f);
	}
	else if (!isAIdle && isBIdle)
	{
		unit->move(-unit->moveSpeed_ * moveDir / 2.f);
	}
	else
	{
		this->move(this->moveSpeed_ * moveDir / 4.f);
		unit->move(-unit->moveSpeed_ * moveDir / 4.f);
	}
#endif
}

void Unit::attack(GameEntity* entity)
{
	entity->takeDamage(attackValue_);
}

void Unit::addPathpoint(sf::Vector2f pathpoint)
{
	path_.push_back(pathpoint);
}
void Unit::clearPath()
{
	path_.clear();
}
sf::Vector2f Unit::getNextPathpoint() const
{
	return path_.front();
}
const std::list<sf::Vector2f>& Unit::getPath() const
{
	return path_;
}

sf::Vector2f Unit::getMovementDirection() const
{
	return VectorMath::normalize(path_.front() - this->getPosition());
}

void Unit::takeStep(const sf::Time& timeStep)
{
	if (path_.empty()) return;
	sf::Vector2f direction = getMovementDirection();
	float distToNextPathPointSquared = VectorMath::lengthSquared(path_.front() - this->getPosition());
	sf::Vector2f step = moveSpeed_ * direction * timeStep.asSeconds();
	float stepLengthSquared = VectorMath::lengthSquared(step);
	if (distToNextPathPointSquared < std::numeric_limits<float>::epsilon() ||
		distToNextPathPointSquared < stepLengthSquared)
	{
		this->setPosition(path_.front());
		path_.pop_front();
	}
	else
	{
		this->move(step);
	}
}

GameEntity* Unit::findNearestEnemyInRange()
{
	GameEntity* nearestEnemy = nullptr;
	float distToNearestEnemySquared = std::numeric_limits<float>::infinity();
	//find nearest enemy that is in range
	for (auto entity : shr_.entityManager.getGameEntities())
	{
		if (this->getOwner() != entity->getOwner() && !entity->isDead())
		{
			float distToUSquared = this->distanceToSquared(*entity);
			if (distToUSquared < attackRange_ * attackRange_ && distToUSquared < distToNearestEnemySquared)
			{
				nearestEnemy = entity;
				distToNearestEnemySquared = distToUSquared;
			}
		}
	}
	return nearestEnemy;
}