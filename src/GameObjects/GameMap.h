#pragma once
#include <cstdint>
#include <vector>
#include <string>
#include <deque>
#include "TerrainTile.h"

/// The terrain types that are allowed on the map.
enum class TerrainType
{
	Undefined = 0,
	Grass,
	Rock,

	Count
};


/// @brief Holds the information on the layout of the game map.
///
/// Most importantly has the information on which areas of the map are passable or not
/// and provides a method that can be used to find a path from point A to point B.
class GameMap
{
public:
	GameMap();
	~GameMap();

	uint64_t getWidth() const;
	uint64_t getHeight() const;

	/// Get the terrain type at a given position.
	/// @param[in] pos The tile's position in linear coordinates.
	TerrainType getTerrainAt(uint64_t pos) const;
	/// Get the terrain type at a given position.
	/// @param[in] x,y The tile's x,y coordinates.
	TerrainType getTerrainAt(uint64_t x, uint64_t y) const;

	/// Check if a tile's passable.
	/// @param[in] pos The tile's position in linear coordinates.
	/// @param[in] width 
	/// \parblock
	/// How many tiles neighboring an impassable tile should be counted as impassable.
	/// Used when determining whether a unit that's wider than a single tile can pass through.
	/// \endparblock
	bool isPassableAt(uint64_t pos, uint64_t width=0) const;

	/// Check if a tile's passable.
	/// @param[in] x,y The tile's x,y coordinates
	/// @param[in] width 
	/// \parblock
	/// How many tiles neighboring an impassable tile should be counted as impassable.
	/// Used when determining whether a unit that's wider than a single tile can pass through.
	/// \endparblock
	bool isPassableAt(uint64_t x, uint64_t y, uint64_t width) const;

	/// Set the passability of a tile.
	/// @param[in] pos The tile's position in linear coordinates.
	/// @param[in] passable The tile's designated passability.
	void setPassability(uint64_t pos, bool passable);

	/// Set the passability of a tile.
	/// @param[in] x,y The tile's x,y coordinates
	/// @param[in] passable The tile's designated passability.
	void setPassability(uint64_t x, uint64_t y, bool passable);

	/// Used to load map information from a text file.
	void loadFromFile(const std::string& fileName);

	/// Converts a tile's position in the map to its position in world space.
	sf::Vector2f gridToWorldCoord(sf::Vector2i gridCoord) const;
	/// Converts a coordinate in world space to the coordinates of a tile it is situated in.
	sf::Vector2i worldToGridCoord(sf::Vector2f worldCoord) const;

	/// Find a path between two points.
	/// @param[in] start The start point of the path.
	/// @param[in] goal The end point of the path.
	/// @param[in] unitWidth The width of the object trying to find its way around the map.
	/// @param[out] path The container in which the path will be stored.
	/// @return Whether a path was found or not.
	bool findPath(sf::Vector2f start, sf::Vector2f goal, std::deque<sf::Vector2f>& path, uint64_t unitWidth=0) const;

private:

	uint64_t width_;
	uint64_t height_;
	std::vector<TerrainType> terrainData_;

	/// Used whenever passability of the map changes. Recalculates tha maps for all used widths.
	void recalculatePassabilityMaps() const;
	void recalculatePassabilityMap(size_t pos) const;

	// stuff below is used for pathfinding. should probably be wrapped in some class.
	float heuristic(sf::Vector2i pos, sf::Vector2i goal) const;
	void simplifyPath(std::deque<sf::Vector2f>& path, uint64_t unitWidth=0) const;
	std::vector<bool> passableMap_;
	mutable std::vector<std::vector<bool>*> passableMaps_;
	std::vector<TerrainType> impassables_ = {
		TerrainType::Rock
	};
	const float tileSize_;

	struct PathfindingTile
	{
		PathfindingTile() :
			fScore(std::numeric_limits<float>::infinity()),
			gScore(std::numeric_limits<float>::infinity()),
			isInOpenSet(false),
			cameFrom(sf::Vector2i(-1, -1))
		{
		}
		float fScore;
		float gScore;
		bool isInOpenSet;
		sf::Vector2i pos;
		sf::Vector2i cameFrom;
	};
};

