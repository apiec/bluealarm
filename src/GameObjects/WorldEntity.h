#pragma once

#include <cstdint>
#include <string>
#include <SFML/Graphics/Sprite.hpp>

/**
 * @brief An WorldEntity is found in the game world.
 *
 * This is mostly a wrapper for the sf::Sprite class with some utility added.
 */
class WorldEntity :
	public sf::Sprite
{
public:
	/// @brief Square of the Euclidean distance to another WorldEntity
	float distanceToSquared(const WorldEntity& object) const;
	/// @brief Euclidean distance to another WorldEntity.
	float distanceTo(const WorldEntity& object) const;
};

