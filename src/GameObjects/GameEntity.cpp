#include "GameEntity.h"
#include "../Macros.h"

GameEntity::GameEntity(SharedResources& shr) :
	owner_(0),
	maxHealth_(100),
	currentHealth_(maxHealth_),
	state_(GameEntityState::Idle),
	shr_(shr)
{
}

GameEntity::~GameEntity()
{
}

void GameEntity::update(const sf::Time& timeStep)
{
	UNUSED(timeStep);
	if (isDead()) return;
	if (deathCondition())
		this->kill();
}

int GameEntity::getState() const
{
	return state_;
}

void GameEntity::setState(int state)
{
	state_ = state;
}

void GameEntity::setOwner(int owner)
{
	if (owner > -1 && owner < teamColors_.size())
	{
		owner_ = owner;
		this->setColor(teamColors_[owner]);
	}
}

int GameEntity::getOwner() const
{
	return owner_;
}

void GameEntity::setMaxHealth(float maxHealth)
{
	maxHealth_ = maxHealth;
}

float GameEntity::getMaxHealth() const
{
	return maxHealth_;
}

void GameEntity::setCurrentHealth(float currentHealth)
{
	if (currentHealth_ > maxHealth_)
		currentHealth_ = maxHealth_;
	else
		currentHealth_ = currentHealth;
}

void GameEntity::changeCurrentHealthBy(float changeValue)
{
	currentHealth_ += changeValue;
	if (currentHealth_ > maxHealth_)
		currentHealth_ = maxHealth_;
}

float GameEntity::getCurrentHealth() const
{
	return currentHealth_;
}

void GameEntity::kill()
{
	state_ = GameEntityState::Dead;
	this->setScale(this->getScale() * -1.f);
}

bool GameEntity::isDead() const
{
	return state_ == GameEntityState::Dead;
}

void GameEntity::takeDamage(float attackValue)
{
	currentHealth_ -= attackValue;
}

bool GameEntity::deathCondition() const
{
	return currentHealth_ <= 0.f;
}