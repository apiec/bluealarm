#pragma once

#include <SFML/System/Vector2.hpp>
#include "../Building.h"

/// @brief A building that can drop atom bombs.
/// An atom bomb damages everything in a radius around a drop point.
/// The damage inflicted falls off with the distance from the centre.
class AtomBombBuilding :
	public Building
{
public:
	AtomBombBuilding(SharedResources& shr);
	void useAtomBomb(sf::Vector2f);

private:
	static constexpr float explosionRadius_ = 400.f;
	static constexpr float explosionDamage_ = 300.f;
};
