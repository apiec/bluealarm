#pragma once
#include "../Unit.h"
#include "../../SharedResources.h"

/// @brief An infantry unit. Heals itself passively.
class Infantry :
    public Unit
{
public:
    Infantry(SharedResources& shr);
    virtual void update(const sf::Time& timeStep) override;

private:
    float healthRegenPerSecond = 1.f;
};

