#pragma once

#include "../Unit.h"
#include "../../SharedResources.h"

/// @brief A plane unit. The plane never stops moving and doesn't change movement direction immediately.
class Plane :
	public Unit
{
public:
	Plane(SharedResources& shr);
	virtual void update(const sf::Time& timeStep) override;

protected:
	/// The plane doesn't just follow the issued path, it has inertia while turning.
	virtual sf::Vector2f getMovementDirection() const override;

private:
	void makeRotationStep(const sf::Time& timeStep);
	void makeIdleStep(const sf::Time& timeStep);
	/// The direction the plane is pointing.
	float pointingAngle_;
	/// The rotational speed in deg/sec
	float rotationSpeed_ = 15.f;
};

