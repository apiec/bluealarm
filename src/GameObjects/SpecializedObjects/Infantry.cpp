#include "Infantry.h"

Infantry::Infantry(SharedResources& shr) :
	Unit(shr)
{
	this->setTexture(shr_.assets.infantryTexture);
	this->recalculateOrigin();
	this->setMoveSpeed(100.f);
	this->setMaxHealth(45.f);
	this->setCurrentHealth(this->getMaxHealth());
	this->setAttackValue(5.f);
	this->setAttackSpeed(.5f);
	this->setAttackRange(150.f);
	this->setFlying(false);
}

void Infantry::update(const sf::Time& timeStep)
{
	this->changeCurrentHealthBy(healthRegenPerSecond * timeStep.asSeconds());
	Unit::update(timeStep);
}