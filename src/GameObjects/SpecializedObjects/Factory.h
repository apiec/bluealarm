#pragma once

#include "../UnitProductionBuilding.h"

/// @brief A production building producing tanks and planes.
class Factory :
	public UnitProductionBuilding
{
public:
	Factory(SharedResources& shr);
	void queueTank();
	void queuePlane();
private:
};

