#include "Barracks.h"
#include "Infantry.h"
#include "../../SharedResources.h"

Barracks::Barracks(SharedResources& shr) :
	UnitProductionBuilding(shr)
{
	this->setTexture(shr_.assets.barracksTexture);
	this->setBuildingOrigin();
}

void Barracks::queueInfantry()
{
	auto inf = new Infantry(shr_);
	inf->setOwner(this->getOwner());
	productionQueue_.addToProductionQueue(inf);
}