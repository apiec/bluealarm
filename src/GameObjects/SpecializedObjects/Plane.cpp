#include "Plane.h"
#include "../../VectorMath.h"
#include <iostream>

Plane::Plane(SharedResources& shr) :
	Unit(shr)
{
	this->setTexture(shr_.assets.planeTexture);
	this->recalculateOrigin();
	this->setMoveSpeed(150.f);
	this->setMaxHealth(120.f);
	this->setCurrentHealth(this->getMaxHealth());
	this->setAttackValue(4.f);
	this->setAttackSpeed(0.1f);
	this->setAttackRange(175.f);
	this->setFlying(true);

	pointingAngle_ = 0.f;
	rotationSpeed_ = 70.f;
}

void Plane::update(const sf::Time& timeStep)
{
	switch (this->getState())
	{
	case UnitState::Idle:
	{
		this->makeRotationStep(timeStep);
		this->makeIdleStep(timeStep);
	}
	break;
	case UnitState::Moving:
	{
		this->makeRotationStep(timeStep);
	}
	break;
	}

	Unit::update(timeStep);
}



void Plane::makeRotationStep(const sf::Time& timeStep)
{
	float rotateStep = rotationSpeed_ * timeStep.asSeconds();
	if (this->getState() == UnitState::Moving)
	{
		auto directionToNextPathpoint = this->getNextPathpoint() - this->getPosition();
		auto angleToNextPathpoint = VectorMath::angle(directionToNextPathpoint);
		auto angleDelta = pointingAngle_ - angleToNextPathpoint;
		if (std::abs(angleDelta) > 180.f)
		{
			angleDelta -= VectorMath::sgn<float>(angleDelta) * 360.f;
		}

		if (std::abs(angleDelta) < rotateStep || std::abs(angleDelta) < 1.f)
		{
			pointingAngle_ = angleToNextPathpoint;
		}
		else
		{
			pointingAngle_ -= rotateStep * VectorMath::sgn<float>(angleDelta);
		}
	}
	else
	{
		pointingAngle_ += rotateStep;
	}

	if (std::abs(pointingAngle_) > 180.f)
	{
		pointingAngle_ -= VectorMath::sgn<float>(pointingAngle_) * 360.f;
	}
}

void Plane::makeIdleStep(const sf::Time& timeStep)
{
	auto step = getMovementDirection() * this->getMoveSpeed() * timeStep.asSeconds();
	this->move(step);
}

sf::Vector2f Plane::getMovementDirection() const
{
	float radAng = pointingAngle_ * VectorMath::PI / 180.f;
	return { std::cosf(radAng), std::sinf(radAng) };
}

