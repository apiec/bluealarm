#include "Tank.h"

Tank::Tank(SharedResources& shr) :
	Unit(shr)
{
	this->setTexture(shr_.assets.tankTexture);
	this->recalculateOrigin();
	this->setScale(1.5f, 1.5f);
	this->setMoveSpeed(70.f);
	this->setMaxHealth(200.f);
	this->setCurrentHealth(this->getMaxHealth());
	this->setAttackValue(35.f);
	this->setAttackSpeed(2.f);
	this->setAttackRange(250.f);
	this->setFlying(false);
}

void Tank::attack(GameEntity* attackedEntity)
{
	for (auto entity : shr_.entityManager.getGameEntities())
	{
		if (attackedEntity->distanceTo(*entity) < splashDamageRadius_)
			entity->takeDamage(this->getAttackValue());
	}
}

void Tank::takeDamage(float attackValue)
{
	Unit::takeDamage(attackValue - armour_);
}