#include "AtomBombBuilding.h"
#include "../../SharedResources.h"
#include "../../VectorMath.h"


AtomBombBuilding::AtomBombBuilding(SharedResources& shr) :
	Building(shr)
{
	this->setTexture(shr_.assets.atomBombBuildingTexture);
	this->setBuildingOrigin();
}

float getDamageAtDistance(float baseDamage, float distance, float maxDistance)
{
	static constexpr float pow = 0.5f;
	float a = -baseDamage / std::powf(maxDistance, pow);
	return a * std::powf(distance, pow) + baseDamage;
}

void AtomBombBuilding::useAtomBomb(sf::Vector2f pos)
{
	for (auto e : shr_.entityManager.getGameEntities())
	{
		float distanceToCentre = VectorMath::length(pos - e->getPosition());
		if (distanceToCentre < explosionRadius_)
		{ 
			e->takeDamage(getDamageAtDistance(explosionDamage_, distanceToCentre, explosionRadius_));
		}
	}
}