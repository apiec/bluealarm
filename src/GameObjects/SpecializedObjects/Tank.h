#pragma once
#include "../Unit.h"
#include "../../SharedResources.h"

/// @brief A tank unit. It has armour and a splash attack.
/// The tank has armour that reduces damage taken and uses a splash attack - damaging
/// all game entities in a radius around its target.
class Tank :
    public Unit
{
public:
    Tank(SharedResources& shr);
    virtual void attack(GameEntity* entity) override;
    /// The tank reduces damage taken by the armour value.
    virtual void takeDamage(float attackValue) override;
protected:
    float armour_ = 3.f;
    float splashDamageRadius_ = 40.f;
};

