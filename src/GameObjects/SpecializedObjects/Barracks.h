#pragma once

#include "../UnitProductionBuilding.h"

/// @brief A production building producing infantry.
class Barracks :
	public UnitProductionBuilding
{
public:
	Barracks(SharedResources& shr);
	void queueInfantry();
};
