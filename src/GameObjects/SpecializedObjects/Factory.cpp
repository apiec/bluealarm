#include "Factory.h"
#include "Tank.h"
#include "Plane.h"
#include "../../SharedResources.h"

Factory::Factory(SharedResources& shr) :
	UnitProductionBuilding(shr)
{
	this->setTexture(shr_.assets.factoryTexture);
	this->setBuildingOrigin();
}

void Factory::queueTank()
{
	auto tank = new Tank(shr_);
	tank->setOwner(this->getOwner());
	productionQueue_.addToProductionQueue(tank);
}

void Factory::queuePlane()
{
	auto plane = new Plane(shr_);
	plane->setOwner(this->getOwner());
	productionQueue_.addToProductionQueue(plane);
}