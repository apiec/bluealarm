#pragma once

#include <functional>

#include "GameEntity.h"
#include "Unit.h"
#include "ProductionQueue.h"

/// @brief A building can be placed on the game map and occupies space there.
class Building :
	public GameEntity
{
public:
	Building(SharedResources& shr);
	~Building();
	/// Place at a point in the game space.
	virtual void placeOnMap(sf::Vector2f);
	/// Removes from map and cleans up after itself.
	void removeFromMap();
	
	/// Sets the origin point to be in the middle of the sprite.
	void setBuildingOrigin();

private:
	/// Holds positions of tiles that the building intersects after being placed.
	std::vector<sf::Vector2i> intersectedTerrainTiles_;
};
