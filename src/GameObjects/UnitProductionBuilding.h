#pragma once

#include "Building.h"

/// A production building.
class UnitProductionBuilding :
	public Building
{
public:
	UnitProductionBuilding(SharedResources& shr);

	/// Has to be called on every game loop.
	virtual void update(const sf::Time& timeStep) override;
	/// Overriden from Building.
	virtual void placeOnMap(sf::Vector2f) override;

	/// Get the point at which units spawn upon being produced.
	sf::Vector2f getSpawnPoint() const;
	
	void setRallyPoint(sf::Vector2f);
	sf::Vector2f getRallyPoint() const;

	/// @return The ratio of the elapsed production time to the total production time.
	float getProductionProgress() const;

protected:
	ProductionQueue<Unit> productionQueue_;
private:
	void spawnUnit(Unit* unit);
	/// The point at which units shall be spawned upon being produced.
	sf::Vector2f spawnPoint_;
	/// The point towards which units are rallied upon being spawned.
	sf::Vector2f rallyPoint_;
};
