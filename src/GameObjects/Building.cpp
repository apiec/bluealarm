#include "Building.h"
#include "../SharedResources.h"
#include "Unit.h"

Building::Building(SharedResources& shr) :
	GameEntity(shr)
{
}

Building::~Building()
{
	this->removeFromMap();
}

void Building::placeOnMap(sf::Vector2f pos)
{
	sf::Vector2i intPos = static_cast<sf::Vector2i>(pos);
	sf::Vector2f buildingPos{ static_cast<float>(intPos.x - intPos.x % TerrainTile::TERRAIN_TILE_SIZE + TerrainTile::TERRAIN_TILE_SIZE / 2),
							  static_cast<float>(intPos.y - intPos.y % TerrainTile::TERRAIN_TILE_SIZE + TerrainTile::TERRAIN_TILE_SIZE / 2) };
	this->setPosition(buildingPos);
	this->setScale(0.9f, 0.9f);
	auto gb = this->getGlobalBounds();
	
	for (auto tt : shr_.entityManager.getTerrainTiles())
	{
		if (gb.intersects(tt->getGlobalBounds()))
		{
			sf::Vector2i ttPos(tt->getPosition());
			int ttSize = TerrainTile::TERRAIN_TILE_SIZE;
			ttPos.x = ttPos.x / ttSize;
			ttPos.y = ttPos.y / ttSize;
			intersectedTerrainTiles_.emplace_back(ttPos);
			shr_.gameMap.setPassability(ttPos.x, ttPos.y, false);
		}
	}
	this->setScale(1.f, 1.f);
	shr_.entityManager.addEntity(this);
}

void Building::removeFromMap()
{
	for (auto ttPos : intersectedTerrainTiles_)
		shr_.gameMap.setPassability(ttPos.x, ttPos.y, true);

	intersectedTerrainTiles_.clear();
}

void Building::setBuildingOrigin()
{
	this->setOrigin(this->getLocalBounds().width / 2.f, this->getLocalBounds().height / 2.f);
}