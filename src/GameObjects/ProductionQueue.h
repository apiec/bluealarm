#pragma once

#include <deque>
#include <SFML/System/Clock.hpp>

/// @brief A timed production queue.
/// A template for things like production buildings, or other production queues found in RTS games.
template <typename T>
class ProductionQueue
{
public:
	~ProductionQueue();

	/// Has to be called on every game loop.
	/// If an entity was produced on the method call returns a pointer to it.
	/// Otherwise returns a nullptr.
	T* update();
	
	/// Check if there's anything in the production queue.
	bool isProducing() const;
	/// @return Const ref to the production queue container.
	const std::deque<std::pair<T*, sf::Time>>& getProductionQueue() const;
	/// @return The elapsed time spent producing the current entity.
	sf::Time getElapsedProductionTime() const;
	/// @return The ratio of the elapsed production time to the total production time.
	float getProductionProgress() const;

	/// Add an entity to the production queue.
	void addToProductionQueue(T*);

	/// Remove the last entity from the production queue.
	void removeFromProductionQueue();
	/// Remove the entity from a specified position from the production queue.
	void removeFromProductionQueue(size_t pos);

private:
	const sf::Time DEFAULT_PRODUCTION_TIME = sf::seconds(5);

	std::deque<std::pair<T*, sf::Time>> productionQueue_;
	sf::Clock productionClock_;
};

template <typename T>
ProductionQueue<T>::~ProductionQueue()
{
	for (auto p : productionQueue_)
		delete p.first;
}

template <typename T>
T* ProductionQueue<T>::update()
{
	if (!productionQueue_.empty() && productionClock_.getElapsedTime() > productionQueue_.front().second)
	{
		auto gameObject = productionQueue_.front().first;
		productionQueue_.pop_front();
		productionClock_.restart();
		return gameObject;
	}
	else
	{
		return nullptr;
	}
}

template <typename T>
void ProductionQueue<T>::addToProductionQueue(T* ptr)
{
	if (productionQueue_.empty())
		productionClock_.restart();

	productionQueue_.emplace_back(std::make_pair(ptr, DEFAULT_PRODUCTION_TIME));
}

template <typename T>
void ProductionQueue<T>::removeFromProductionQueue()
{
	if (productionQueue_.empty()) return;

	delete productionQueue_.back().first;
	productionQueue_.pop_back();
}

template <typename T>
void ProductionQueue<T>::removeFromProductionQueue(size_t pos)
{
	if (productionQueue_.empty()) return;

	if (pos >= productionQueue_.size())
		return;
	else
	{
		delete productionQueue_[pos].first;
		productionQueue_.erase(std::next(productionQueue_.begin(), pos));
	}
}

template <typename T>
const std::deque<std::pair<T*, sf::Time>>& ProductionQueue<T>::getProductionQueue() const
{
	return productionQueue_;
}

template <typename T>
sf::Time ProductionQueue<T>::getElapsedProductionTime() const
{
	return productionClock_.getElapsedTime();
}

template <typename T>
float ProductionQueue<T>::getProductionProgress() const
{
	if (!productionQueue_.empty())
	{
		return productionClock_.getElapsedTime().asSeconds() / productionQueue_.front().second.asSeconds();
	}
	else
	{
		return 0;
	}
}

template <typename T>
bool ProductionQueue<T>::isProducing() const
{
	return !productionQueue_.empty();
}
