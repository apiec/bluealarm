#include "App.h"
#include "Macros.h"
#include <thread>
#include <iostream>

App::App() :
	shr_(),
	objectManager_(shr_),
	renderingManager_(shr_, &window_),
	eventDispatcher_(&window_),
	window_(sf::VideoMode(800, 600), "MyWindow")
{
	eventDispatcher_.connect(sf::Event::Closed,
		[this](const sf::Event& e)
		{
			UNUSED(e);
			window_.close();
		});
	eventDispatcher_.connect(sf::Event::MouseMoved,
		[this](const sf::Event& e)
		{
			renderingManager_.onMouseMoved(e);
		});
	eventDispatcher_.connect(sf::Event::MouseWheelScrolled,
		[this](const sf::Event& e)
		{
			renderingManager_.onMouseWheelScrolled(e);
		});
	eventDispatcher_.connect(sf::Event::Resized,
		[this](const sf::Event& e)
		{
			renderingManager_.onResized(e);
		});
	eventDispatcher_.connect(sf::Event::KeyPressed,
		[this](const sf::Event& e)
		{
			objectManager_.onKeyPressed(e);
			shr_.inputManager.onPressedEvent(e);
		});
	eventDispatcher_.connect(sf::Event::MouseButtonPressed,
		[this](const sf::Event& e)
		{
			renderingManager_.onMouseButtonPressed(e);
			sf::Vector2i windowCoords(e.mouseButton.x, e.mouseButton.y);
			sf::Vector2f worldCoords = renderingManager_.mapPixelToCoords(windowCoords);
			sf::Event fakeEvent = e;
			fakeEvent.mouseButton.x = static_cast<int>(worldCoords.x);
			fakeEvent.mouseButton.y = static_cast<int>(worldCoords.y);
			objectManager_.onMouseButtonPressed(fakeEvent);
			shr_.inputManager.onPressedEvent(e);
		});
	eventDispatcher_.connect(sf::Event::MouseButtonReleased,
		[this](const sf::Event& e)
		{
			renderingManager_.onMouseButtonReleased(e);
			sf::Vector2i windowCoords(e.mouseButton.x, e.mouseButton.y);
			sf::Vector2f worldCoords = renderingManager_.mapPixelToCoords(windowCoords);
			sf::Event fakeEvent = e;
			fakeEvent.mouseButton.x = static_cast<int>(worldCoords.x);
			fakeEvent.mouseButton.y = static_cast<int>(worldCoords.y);
			objectManager_.onMouseButtonReleased(fakeEvent);
		});

}

void App::start()
{
	mainLoop();
}

void App::mainLoop()
{
	while(window_.isOpen())
	{
		eventDispatcher_.loop();
		objectManager_.loop();
		renderingManager_.loop();
	}
}

