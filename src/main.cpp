#include "App.h"
#include "Macros.h"

int main()
{
	App app;
	app.start();
	return 0;
}